package com.example.dom4.rv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dom4.R;
import com.example.dom4.Visit;

import java.util.ArrayList;
import java.util.List;

public class VisitAdapter extends RecyclerView.Adapter<VisitViewHolder> {

    private List<Visit> visits = new ArrayList<>();

    @NonNull
    @Override
    public VisitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.rv_card, parent, false);
        return new VisitViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitViewHolder holder, int position) {
        holder.setContent(visits.get(position));
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
