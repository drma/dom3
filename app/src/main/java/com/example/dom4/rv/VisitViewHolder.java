package com.example.dom4.rv;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dom4.R;
import com.example.dom4.Visit;

public class VisitViewHolder extends RecyclerView.ViewHolder {
    private TextView locationTv;
    private TextView timeTv;
    private TextView descriptionTv;

    public VisitViewHolder(@NonNull View itemView) {
        super(itemView);

        locationTv = itemView.findViewById(R.id.rv_card_location);
        timeTv = itemView.findViewById(R.id.rv_card_time);
        descriptionTv = itemView.findViewById(R.id.rv_card_description);
    }

    public boolean setContent(Visit visit) {
        locationTv.setText(visit.getLocation().toString());
        timeTv.setText(visit.getDate().toString());
        descriptionTv.setText(visit.getDescription());
        return true;
    }
}
