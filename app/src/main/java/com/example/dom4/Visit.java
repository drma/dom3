package com.example.dom4;

import android.location.Location;

import java.util.Date;

public class Visit {

    private String description;
    private Date date;
    private Location location;

    public Visit(String description, Date date, Location location) {
        this.description = description;
        this.date = date;
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public Date getDate() {
        return date;
    }

    public Location getLocation() {
        return location;
    }
}
